﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary 
{
	public float xMin, xMax, zMin, zMax;
}
public class PlayerController : MonoBehaviour 
{

	public float speed;
	public float tilt;
	public Boundary boundary;
	private Rigidbody rb;
	public GameObject shot;
	public GameObject shot1;
	public GameObject shot2;
	public GameObject shot3;
	public Transform shotSpawn;
	public Transform shotSpawnLeft;
	public Transform shotSpawnLeft2;
	public Transform shotSpawnRight;
	public Transform shotSpawnRight2;
	public float fireRate;
	private float nextFire;
	private AudioSource audioSource;

	void Start () {
		rb = GetComponent<Rigidbody> ();
		audioSource = GetComponent<AudioSource> ();
	}

	void Update()
	{
		if (Input.GetButton("Fire1") && Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;
			//GameObject clone = 
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation); // as GameObject;
			Instantiate (shot2, shotSpawnLeft.position, shotSpawnLeft.rotation); // as GameObject;
			Instantiate (shot, shotSpawnRight.position, shotSpawnRight.rotation); // as GameObject;
			Instantiate (shot3, shotSpawnLeft2.position, shotSpawnLeft2.rotation); // as GameObject;
			Instantiate (shot3, shotSpawnRight2.position, shotSpawnRight2.rotation); // as GameObject;
			audioSource.Play ();
		}
	}
	void FixedUpdate()

	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.velocity = movement * speed;

		rb.position = new Vector3 (
			Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax),
			0.0f,
			Mathf.Clamp (rb.position.z, boundary.zMin, boundary.zMax)
		);
		rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);
	}
}
