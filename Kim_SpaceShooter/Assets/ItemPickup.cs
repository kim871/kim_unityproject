﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour 
{
	private Rigidbody rb;
	public GameObject explosion;
	public int scoreValue;
	private GameController gameController;

	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent <GameController> ();
		}
		if (gameController == null) {
			Debug.Log ("Cannot find 'GameController' script");
		}
		rb = GetComponent<Rigidbody> ();
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			Instantiate (explosion, other.transform.position, other.transform.rotation);
			gameController.AddScore (scoreValue);
			Destroy (gameObject);
		}
	}
}
